module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'ReduxFirstRouterPage',
      externals: {
        react: 'React'
      }
    }
  }
}
