module.exports = {
  extends: 'airbnb',
  plugins: ['import'],
  env: {
    browser: true,
    node: true,
  },
  rules: {
    'import/extensions': 0,
    'import/no-absolute-path': 0,
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    indent: ['error', 2],
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
  },
};
