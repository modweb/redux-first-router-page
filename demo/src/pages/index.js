import { PageOne } from './PageOne';
import { PageTwo } from './PageTwo';

export default {
  PAGEONE: PageOne,
  PAGETWO: PageTwo,
};
