import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { connectRoutes } from 'redux-first-router';
import Link from 'redux-first-router-link';
import createHistory from 'history/createBrowserHistory';

import pages from './pages';
import Page from '../../src';

const history = createHistory();

const routesMap = {
  PAGEONE: '/',
  PAGETWO: '/page-two',
};

const { reducer, middleware, enhancer } = connectRoutes(history, routesMap);

const rootReducer = combineReducers({ location: reducer });
const middlewares = applyMiddleware(middleware);
const store = createStore(rootReducer, compose(enhancer, middlewares));

const Demo = () =>
  (<Provider store={store}>
    <div>
      <h1>redux-first-router-page Demo</h1>
      <div>
        <Link to={{ type: 'PAGEONE', payload: {} }}>Page One (/)</Link>
      </div>
      <div>
        <Link to={{ type: 'PAGETWO', payload: {} }}>Page Two (/page-two)</Link>
      </div>
      <Page pages={pages} />
    </div>
  </Provider>);

render(<Demo />, document.querySelector('#demo'));
