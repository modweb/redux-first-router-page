# redux-first-router-page

Simple redux connected `Page` component designed to work flawlessly with `redux-first-router`;

## Fancy Gif

![Fancy Gif](https://gitlab.com/modweb/redux-first-router-page/raw/master/react-redux-router-page.gif)

## The Goods

```jsx
import React from 'react';
import { connect } from 'react-redux';
import { string, object } from 'prop-types';

const Page = ({ type, pages }) => {
  const CurrentPageComponent = pages[type];
  return CurrentPageComponent ? <CurrentPageComponent /> : null;
};

Page.propTypes = {
  type: string.isRequired,
  pages: object.isRequired,
};

const mapStateToProps = ({ location: { type } }) => ({ type });
export default connect(mapStateToProps)(Page);
```

## Example

pages/index.js
```js
import { PageOne } from './PageOne';
import { PageTwo } from './PageTwo';

export default {
  PAGEONE: PageOne,
  PAGETWO: PageTwo
};
```

pages/pageOne/index.js
```jsx
import React from 'react';

export const PageOne = () => <h2>Page One</h2>;
```

pages/pageTwo/index.js
```jsx
import React from 'react';

export const PageTwo = () => <h2>Page Two</h2>;
```

app.js
```jsx
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { connectRoutes } from 'redux-first-router';
import Link from 'redux-first-router-link';
import createHistory from 'history/createBrowserHistory';

import pages from './pages';
import Page from 'redux-first-router-page';

const history = createHistory();

const routesMap = {
  PAGEONE: '/',
  PAGETWO: '/page-two',
};

const { reducer, middleware, enhancer } = connectRoutes(history, routesMap);

const rootReducer = combineReducers({ location: reducer });
const middlewares = applyMiddleware(middleware);
const store = createStore(rootReducer, compose(enhancer, middlewares));

const Demo = () =>
  (<Provider store={store}>
    <div>
      <h1>redux-first-router-page Demo</h1>
      <div>
        <Link to={{ type: 'PAGEONE', payload: {} }}>Page One (/)</Link>
      </div>
      <div>
        <Link to={{ type: 'PAGETWO', payload: {} }}>Page Two (/page-two)</Link>
      </div>
      <Page pages={pages} />
    </div>
  </Provider>);

render(<Demo />, document.querySelector('#demo'));
```
