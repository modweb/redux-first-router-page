/* eslint react/forbid-prop-types: 0 */
import React from 'react';
import { connect } from 'react-redux';
import { string, object } from 'prop-types';

const Page = ({ type, pages }) => {
  const CurrentPageComponent = pages[type];
  return CurrentPageComponent ? <CurrentPageComponent /> : null;
};

Page.propTypes = {
  type: string.isRequired,
  pages: object.isRequired,
};

const mapStateToProps = ({ location: { type } }) => ({ type });
export default connect(mapStateToProps)(Page);
